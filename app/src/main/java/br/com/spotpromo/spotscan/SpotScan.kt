package br.com.spotpromo.spotscan

import android.Manifest
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import br.com.spotpromo.spotscan.configuracao.Configuracao
import br.com.spotpromo.spotscan.utils.Alerta
import com.google.zxing.ResultPoint
import com.journeyapps.barcodescanner.BarcodeCallback
import com.journeyapps.barcodescanner.BarcodeResult
import kotlinx.android.synthetic.main.layout_spot_scan.*
import kotlinx.android.synthetic.main.layout_camera_scan.*

class SpotScan : AppCompatActivity() {

    private var mClasse: Class<*>? = null
    private var mTextScan: String? = ""

    private val CAMERA_REQUEST_CODE = 100

    val callback = object : BarcodeCallback {
        override fun barcodeResult(result: BarcodeResult) {

            try {
                zxing_barcode_scanner?.pause()
                if (result.text != null) {
                    mTextScan = result.text
                    //Alerta.show(this@SpotScan, "Text", result.text, "OK", DialogInterface.OnClickListener { dialogInterface, i ->  onResume()}, false)
                    onFinish()
                } else {
                    onError()
                }
            } catch (e: Exception) {
                onError()
            }
        }

        override fun possibleResultPoints(resultPoints: List<ResultPoint>) {}
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            val w: Window = window
            w.setFlags(
                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
            )
        }

        setContentView(R.layout.layout_spot_scan)

        onSetConfigs()
        onCheckPermission()
    }

    private fun onSetConfigs() {
        zxing_viewfinder_view.setMaskColor(Color.parseColor(Configuracao.color))
    }

    private fun onCheckPermission() {
        if (ContextCompat.checkSelfPermission(
                this@SpotScan,
                Manifest.permission.CAMERA
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            Alerta.show(
                this@SpotScan,
                resources.getString(R.string.msg_atencao),
                resources.getString(R.string.msg_permissao_camera),
                resources.getString(R.string.btn_permitir),
                DialogInterface.OnClickListener { dialogInterface, i ->
                    ActivityCompat.requestPermissions(
                        this@SpotScan,
                        arrayOf(Manifest.permission.CAMERA),
                        CAMERA_REQUEST_CODE
                    )
                },
                resources.getString(R.string.btn_cancelar),
                DialogInterface.OnClickListener { dialogInterface, i -> onBackPressed() }, false
            )
            return
        }


        onResume()
    }

    private fun onStartCameraScan() {

        zxing_barcode_scanner.decodeSingle(callback)
        zxing_barcode_scanner.setStatusText("")
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        onCheckPermission()

    }

    override fun onResume() {
        super.onResume()
        onStartCameraScan()
        zxing_barcode_scanner.resume()
    }

    override fun onBackPressed() {
        super.onBackPressed()

        setResult(Configuracao.COD_RESULT_SCAN_CANCELED)
        onCloseScan()
    }

    private fun onFinish() {

        var intent = Intent()
        intent.putExtra("datascan", mTextScan)
        setResult(Configuracao.COD_RESULT_SCAN, intent)
        onCloseScan()
    }

    private fun onError() {
        setResult(Configuracao.COD_RESULT_SCAN_ERROR)
        onCloseScan()
    }

    private fun onCloseScan() {
        finish()
    }
}